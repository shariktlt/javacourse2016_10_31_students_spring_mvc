package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.StudentDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.MapperFactoryInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.StudentServiceInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.LessionEntity;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.StudentEntity;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.UserEntity;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.factory.MapperFactoryBean;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.repository.StudentRepository;

import java.util.List;

/**
 * Created by innopolis on 31.10.16.
 */
@Service
@Qualifier("studentService")
public class StudentService  implements StudentServiceInterface{
    @Autowired
    StudentRepository studentDAO;

    MapperFactoryInterface mapperFactory;
    MapperFacade mapperFacade;

    @Autowired
    public void setMapperFactory(MapperFactoryInterface mapperFactory){
        this.mapperFactory = mapperFactory;
        mapperFactory.getFactory().classMap(StudentEntity.class, Student.class)
                .byDefault()
                .register();
        mapperFactory.getFactory().classMap(StudentDTO.class, StudentEntity.class)
                .byDefault()
                .register();
        mapperFacade = mapperFactory.getFactory().getMapperFacade();
    }

    @Override
    public List<Student> getList(ListDTO listDTO){

        Iterable<StudentEntity> list = studentDAO.findAll();

        //Page<StudentEntity> studentEntityPage = studentDAO.findByFirstName(listDTO.getNameFilter(), new PageRequest(Math.toIntExact(listDTO.getOffset()), 20)); //studentDAO.getList(listDTO);
        return mapperFacade.mapAsList(list, Student.class);
    }

    @Override
    public Student getById(Long id) {
        Student student = mapperFacade.map(studentDAO.getById(id), Student.class);
        if(student == null){
            student = new Student();
            student.setFirstName("");
            student.setLastName("");
            student.setSex("");
            student.setBirth("");
            student.setId(0);
        }
      /*  if(student.getLessionList()!=null){
            for(Lession lession: student.getLessionList()){
                student.getLessionListIdMap().put(lession.getId(), true);
            }
        }*/
        return student;
    }

    @Override
    public Long save(Long id, StudentDTO studentDTO) {
        StudentEntity student = studentDAO.save(mapperFacade.map(studentDTO, StudentEntity.class));
        return student.getId();
    }

    @Override
    public boolean delete(Long id) {
        boolean res = false;
        studentDAO.delete(id);
        res = true;
        return res;
    }
}
