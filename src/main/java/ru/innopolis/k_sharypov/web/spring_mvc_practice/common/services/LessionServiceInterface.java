package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;

import java.util.List;

/**
 * Created by innopolis on 31.10.16.
 */
public interface LessionServiceInterface {
    /**
     *  Return list by listDTO
     * @param listDTO
     * @return list of Lession
     */
    List<Lession> getList(ListDTO listDTO);

    /**
     * Return student by id
     * @param id
     * @return Student entity
     */
    Lession getById(Long id);

    /**
     * Add or update student by id, id=0 - cause adding new
     * @param id
     * @param lessionDTO
     * @return id of saving row
     */
    Long save(Long id, LessionDTO lessionDTO);

    /**
     * Delete student by id
     * @param id
     * @return isSuccess
     */
    boolean delete(Long id);
}
