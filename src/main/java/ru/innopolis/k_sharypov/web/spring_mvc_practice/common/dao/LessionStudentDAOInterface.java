package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;

import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
public interface LessionStudentDAOInterface  {
    /**
     * Return list of Student by LessionId
     * @param id
     * @return List<Student>
     */
    List<Student> getStudentsByLessionId(Long id);

    /**
     *  Return list of Lession by StudentId
     * @param id
     * @return List<Lession>
     */
    List<Lession> getLessionByStudentId(Long id);

    /**
     * Return count of student lessions
     * @param id
     * @return
     */
    Long getStudentLessionsCount(Long id);

    void fillStudent(Long id, String[] lessionIds);
}
