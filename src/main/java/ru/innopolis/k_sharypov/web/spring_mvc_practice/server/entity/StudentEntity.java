package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by innopolis on 09.11.16.
 */
@Entity
@Table (name = "students")
public class StudentEntity {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name ="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="sex")
    private String sex;

    @Column(name ="birth")
    private Date birth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }
}
