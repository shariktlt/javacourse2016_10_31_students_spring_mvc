package ru.innopolis.k_sharypov.web.spring_mvc_practice.client.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.LessionServiceInterface;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by innopolis on 01.11.16.
 */
@Controller
@RequestMapping("/app/lessions")
@PreAuthorize("isAuthenticated()")
public class LessionController {
    private static Logger logger = LoggerFactory.getLogger(StudentController.class);
    @Autowired
    LessionServiceInterface lessionService;

    @RequestMapping("/")
    public ModelAndView list(HttpServletRequest req){
        ModelAndView modelAndView = new ModelAndView("lession/list");
        ListDTO listDTO = getRequestListTDO(req);
        modelAndView.addObject("list", lessionService.getList(listDTO));
        return modelAndView;
    }
    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public ModelAndView view(HttpServletRequest req, @PathVariable Long id){
        logger.info("Show {}",id);
        ModelAndView mv = new ModelAndView("lession/form");
        mv.addObject("lession", lessionService.getById(id));
        return mv;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/view/{id}", method = RequestMethod.POST)
    public String update(HttpServletRequest req, @PathVariable Long id){
        logger.info("Update {}",id);
        LessionDTO lessionDTO = getRequestLessionDTO(req);
        Long studentId = lessionService.save(id, lessionDTO);
        return "redirect:/app/lessions/view/"+ studentId;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/view/{id}/del")
    public String delete(@PathVariable Long id){
        logger.info("Delete {}",id);
        lessionService.delete(id);
        return "redirect:/app/lessions/";
    }

    private LessionDTO getRequestLessionDTO(HttpServletRequest req) {
        LessionDTO s = new LessionDTO();
        /**
         *  For propertly process non asci symbols in URI, add  URIEncoding="UTF-8" to <connector> in TOMCAT_DIR/conf/server.xml
         */
        s.setTopic(req.getParameter("topic"));
        s.setDescription(req.getParameter("description"));
        s.setDuration(req.getParameter("duration"));
        s.setDate(req.getParameter("date"));
        return s;
    }

    private ListDTO getRequestListTDO(HttpServletRequest req){
        return ListDTO.getListDTO(req);
    }


}
