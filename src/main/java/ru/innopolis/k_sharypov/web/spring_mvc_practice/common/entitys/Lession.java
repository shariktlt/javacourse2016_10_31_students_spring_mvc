package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
public class Lession {
    private Long id;
    private String topic;
    private String description;
    private Long duration;
    private String date;
    private List<Student> studentList;

    public Lession() {
        studentList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
