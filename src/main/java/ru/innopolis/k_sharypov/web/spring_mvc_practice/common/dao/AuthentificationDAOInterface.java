package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao;

import java.util.List;

/**
 * Created by innopolis on 03.11.16.
 */
public interface AuthentificationDAOInterface {
    List<String> getRolesByLoginPassword(String login, String password);

    Long registerByLoginPassword(String login, String pass);
}
