package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.mapper.converter;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by innopolis on 07.11.16.
 */
@Component
public class StringToDateConverter extends BidirectionalConverter<String,Date> {
    @Override
    public Date convertTo(String source, Type<Date> destinationType) {
        Date d = null;
        try {
            d = new Date(new SimpleDateFormat("dd-MM-yyyy").parse(source).getTime());
        } catch (ParseException e) {

        }
        return d;
    }

    @Override
    public String convertFrom(Date source, Type<String> destinationType) {
        return new SimpleDateFormat("dd-MM-yyyy").format(source);
    }
}
