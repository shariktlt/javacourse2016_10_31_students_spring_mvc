package ru.innopolis.k_sharypov.web.spring_mvc_practice.client.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.LessionServiceInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.StudentServiceInterface;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by innopolis on 31.10.16.
 */
@Controller
@RequestMapping("/app/student")
@PreAuthorize("isAuthenticated()")
public class StudentController {
    private static Logger logger = LoggerFactory.getLogger(StudentController.class);


    @Autowired
    @Qualifier("studentServiceProxy")
    private StudentServiceInterface studentService;

    @Autowired
    private LessionServiceInterface lessionService;


    @RequestMapping("/")
    public ModelAndView list(HttpServletRequest req){
        ModelAndView  mv = new ModelAndView();
        ListDTO listDTO = getRequestListTDO(req);
        mv.setViewName("student/list");
        mv.addObject("listDTO", listDTO);
        mv.addObject("list", studentService.getList(listDTO));
        return mv;
    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public ModelAndView view(HttpServletRequest req, @PathVariable Long id){
        logger.info("Show {}",id);
        ModelAndView mv = new ModelAndView("student/form");
        mv.addObject("student", studentService.getById(id));
        mv.addObject("lessions", lessionService.getList(getRequestListTDO(req)));
        return mv;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/view/{id}", method = RequestMethod.POST)
    public String update(HttpServletRequest req, @PathVariable Long id){
        logger.info("Update {}",id);
        StudentDTO studentDTO = getRequestStudentDTO(req);
        Long studentId = studentService.save(id, studentDTO);
        return "redirect:/app/student/view/"+ studentId;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/view/{id}/del")
    public String delete(@PathVariable Long id){
        logger.info("Delete {}",id);
        studentService.delete(id);
        return "redirect:/app/student/";
    }

    private StudentDTO getRequestStudentDTO(HttpServletRequest req) {
        StudentDTO s = new StudentDTO();
        /**
         *  For propertly process non asci symbols in URI, add  URIEncoding="UTF-8" to <connector> in TOMCAT_DIR/conf/server.xml
         */
        s.setFirstName(req.getParameter("firstName"));
        s.setLastName(req.getParameter("lastName"));
        s.setSex(req.getParameter("sex"));
        s.setBirth(req.getParameter("birth"));
        s.setLessionIds(req.getParameterValues("lessions_id"));
        return s;
    }

    private ListDTO getRequestListTDO(HttpServletRequest req){
        return ListDTO.getListDTO(req);
    }

}
