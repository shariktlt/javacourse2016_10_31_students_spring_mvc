package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.AuthentificationDAOInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 03.11.16.
 */
@Service
public class AuthentificationProviderService implements AuthenticationProvider {

    @Autowired
    AuthentificationDAOInterface authentificationDAOInterface;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if(authentication.getName() != null && authentication.getCredentials() != null){
            authentication = processLoginPass(authentication);
        }else{
            authentication = null;
        }
        return authentication;
    }

    private Authentication processLoginPass(Authentication authentication) {
        Authentication res = authentication;
        Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
        String name = authentication.getName();
        String password = passwordEncoder.encodePassword(authentication.getCredentials().toString(), "salt");
        List<String> roles = authentificationDAOInterface.getRolesByLoginPassword(name, password);
        if (roles != null && roles.size() > 0) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            for(String role: roles){
                grantedAuths.add(new SimpleGrantedAuthority(role));
            }

            authentication = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);

        }else{
            throw new BadCredentialsException("No user found");
        }
            return authentication;

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public Long register(String login, String pass) {
        Long res = null;
        if(login != null && !login.isEmpty() && pass != null && !pass.isEmpty()){
            res = doRegister(login, pass);
        }
        return res;
    }

    private Long doRegister(String login, String pass) {
        return authentificationDAOInterface.registerByLoginPassword(login, pass);
    }
}
