package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
public interface LessionDAOInterface {

    /**
     * Save lession (or update if exist)
     * @param lession
     * @return
     */
    Long save(LessionDTO lession);

    /**
     *  Return list by ListDTO statements
     * @param listDTO
     * @return
     */
    List<Lession> getList(ListDTO listDTO);

    /**
     * Return LessionController by id
     * @param id
     * @return Lession
     */
    Lession getById(Long id);

    /**
     * Insert LessionController to DB
     * @param lessionDTO
     * @return Long id
     */
    Long add(LessionDTO lessionDTO);

    /**
     * Update LessionController
     * @param lessionDTO
     * @return Long id
     */
    Long update(Long id, LessionDTO lessionDTO);


    /**
     * Delete LessionController by id
     * @param id
     * @return boolean isSuccess
     */
    boolean deleteById(Long id);

    /**
     * Build lession entity
     * @param rs ResultSet
     * @param isNeedJoin
     * @return Lession
     * @throws SQLException
     */
    Lession buildLession(ResultSet rs, boolean isNeedJoin) throws SQLException;
}
