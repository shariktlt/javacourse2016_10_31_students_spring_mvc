package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory;

import javax.persistence.EntityManager;

/**
 * Created by innopolis on 07.11.16.
 */
public interface EntityManagerFactoryInterface {
    EntityManager getEm();
}
