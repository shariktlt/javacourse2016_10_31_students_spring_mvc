package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys;

/**
 * Created by innopolis on 06.11.16.
 */
public class User {
    private String login;
    private String password;
    private String roles;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
