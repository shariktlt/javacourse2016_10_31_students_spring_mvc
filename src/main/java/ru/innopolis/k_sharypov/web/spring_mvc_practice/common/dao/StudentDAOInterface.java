package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by innopolis on 31.10.16.
 */
public interface StudentDAOInterface {
    /**
     *  Return list by ListDTO statements
     * @param listDTO
     * @return
     */
    List<Student> getList(ListDTO listDTO);

    /**
     * Return student by id
     * @param id
     * @return Student
     */
    Student getById(Long id);

    /**
     * Insert student to DB
     * @param studentDTO
     * @return Long id
     */
    Long add(StudentDTO studentDTO);

    /**
     * Update student
     * @param studentDTO
     * @return Long id
     */
    Long update(Long id, StudentDTO studentDTO);


    /**
     * Delete student by id
     * @param id
     * @return boolean isSuccess
     */
    boolean deleteById(Long id);

    /**
     *  build Student entity
     * @param rs
     * @param isNeedJoin
     * @return
     * @throws SQLException
     */
    Student buildStudent(ResultSet rs, boolean isNeedJoin) throws SQLException;
}
