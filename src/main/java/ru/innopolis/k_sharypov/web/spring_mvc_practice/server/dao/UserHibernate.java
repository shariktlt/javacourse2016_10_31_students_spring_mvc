package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.AuthentificationDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.EntityManagerFactoryInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.UserEntity;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 07.11.16.
 */
@Repository
public class UserHibernate implements AuthentificationDAOInterface {

    @Autowired
    private EntityManagerFactoryInterface entity_manager_factory;

    @Override
    public List<String> getRolesByLoginPassword(String login, String password) {
        EntityManager entityManager = entity_manager_factory.getEm();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> criteria = cb.createQuery( UserEntity.class );
        Root<UserEntity> personRoot = criteria.from( UserEntity.class );
        criteria.select( personRoot );
        criteria.where(
                cb.and(
                        cb.equal( personRoot.get( "login" ), login ),
                        cb.equal(personRoot.get("password"), password)
                )
        );
        List<UserEntity> list = entityManager.createQuery(criteria).getResultList();
        List<String> roles = new ArrayList<>();
        for(UserEntity user: list){
            roles.add(user.getRole());
        }
        return roles;
    }

    @Override
    public Long registerByLoginPassword(String login, String pass) {
        return null;
    }
}
