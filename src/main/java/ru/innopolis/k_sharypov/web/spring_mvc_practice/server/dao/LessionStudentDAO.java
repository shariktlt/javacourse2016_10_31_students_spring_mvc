package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.LessionDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.LessionStudentDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.StudentDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
@Repository
public class LessionStudentDAO implements LessionStudentDAOInterface {
    private static Logger logger = LoggerFactory.getLogger(LessionStudentDAO.class);
    @Autowired
    DbPool dbPool;

   // @Autowired
    StudentDAOInterface studentDAO;

   // @Autowired
    LessionDAOInterface lessionDAO;


    @Override
    public List<Student> getStudentsByLessionId(Long id) {
        List<Student> list = new ArrayList<>();
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("SELECT s.id, s.first_name, s.last_name, s.sex, s.birth FROM lessions_student ls LEFT JOIN  students s ON ls.student_id=s.id WHERE ls.lession_id = ?")) {
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(studentDAO.buildStudent(rs, false));
            }
        } catch (SQLException e) {
            logger.error("getById exception",e);
        }
        return list;
    }

    @Override
    public List<Lession> getLessionByStudentId(Long id) {
        List<Lession> list = new ArrayList<>();
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("SELECT l.id, l.topic, l.description, l.duration, l.date FROM lessions_student ls LEFT JOIN  lession l ON ls.lession_id=l.id WHERE ls.student_id = ?")) {
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(lessionDAO.buildLession(rs,false));
            }
        } catch (SQLException e) {
            logger.error("getById exception",e);
        }
        return list;
    }

    @Override
    public Long getStudentLessionsCount(Long id) {
        Long count = null;
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("SELECT count(1) FROM lessions_student ls LEFT JOIN  lession l ON ls.lession_id=l.id WHERE ls.student_id = ?")) {
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                count = rs.getLong(1);
            }
        } catch (SQLException e) {
            logger.error("getById exception",e);
        }
        return count;
    }

    @Override
    public void fillStudent(Long id, String[] lessionIds) {
        try(Connection conn = dbPool.getConn();
            PreparedStatement st = conn.prepareStatement("DELETE FROM lessions_student WHERE student_id = ?")){
            st.setLong(1, id);
            st.execute();
        } catch (SQLException e) {
            logger.error("fillStudent exception:", e);
        }
        try(Connection conn = dbPool.getConn();
            PreparedStatement st = conn.prepareStatement("INSERT INTO lessions_student (lession_id, student_id) VALUES (?,?)")){
            st.setLong(2, id);
            for(String lessionId: lessionIds){
                st.setLong(1, new Long(lessionId));
                st.execute();
            }
        } catch (SQLException e) {
            logger.error("fillStudent exception:", e);
        }
    }
}
