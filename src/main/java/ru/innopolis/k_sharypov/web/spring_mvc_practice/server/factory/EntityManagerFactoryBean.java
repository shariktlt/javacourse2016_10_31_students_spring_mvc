package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.EntityManagerFactoryInterface;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by innopolis on 07.11.16.
 */
@Component("entity_manager_factory")
public class EntityManagerFactoryBean implements EntityManagerFactoryInterface {
    private final String PERSISTENCE_UNIT_NAME = "org.hibernate.mvc.jpa";

    private EntityManagerFactory emf;

    @Autowired
    public void setEntityManager(EntityManagerFactory emf){
        this.emf = emf;
    }

    public EntityManager getEm(){
        return emf.createEntityManager();
    }
}
