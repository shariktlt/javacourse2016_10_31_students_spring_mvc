package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by innopolis on 31.10.16.
 */
public class Student {
    private long id;
    private String firstName;
    private String lastName;
    private String sex;
    private String birth;
    private List<Lession> lessionList;
    private Map<Long,Boolean> lessionListIdMap;
    private String[] lessionIds;
    private Long lessionsCount;

    public Student() {
        lessionListIdMap = new HashMap<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Lession> getLessionList() {
        return lessionList;
    }

    public void setLessionList(List<Lession> lessionList) {
        this.lessionList = lessionList;
    }

    public Long getLessionsCount() {
        return lessionsCount;
    }

    public void setLessionsCount(Long lessionsCount) {
        this.lessionsCount = lessionsCount;
    }

    public Map<Long, Boolean> getLessionListIdMap() {
        return lessionListIdMap;
    }

    public void setLessionListIdMap(Map<Long, Boolean> lessionListIdMap) {
        this.lessionListIdMap = lessionListIdMap;
    }

    public String[] getLessionIds() {
        return lessionIds;
    }

    public void setLessionIds(String[] lessionIds) {
        this.lessionIds = lessionIds;
    }
}
