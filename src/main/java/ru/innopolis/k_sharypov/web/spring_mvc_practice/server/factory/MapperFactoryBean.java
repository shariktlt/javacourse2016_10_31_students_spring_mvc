package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.factory;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.MapperFactoryInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.mapper.converter.StringToDateConverter;

/**
 * Created by innopolis on 07.11.16.
 */
@Component
public class MapperFactoryBean implements MapperFactoryInterface {
    MapperFactory mf;
    {
        mf = new DefaultMapperFactory.Builder().build();
    }

    /**
     * Return MapperFactory
     * @return
     */
    public MapperFactory getFactory(){
        return mf;
    }

    @Autowired
    public void registerDateConvertor(StringToDateConverter conv){
        mf.getConverterFactory().registerConverter(conv);
    }
}
