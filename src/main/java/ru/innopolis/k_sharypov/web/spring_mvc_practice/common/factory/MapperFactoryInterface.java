package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory;

import ma.glasnost.orika.MapperFactory;

/**
 * Created by innopolis on 07.11.16.
 */
public interface MapperFactoryInterface {
    MapperFactory getFactory();
}
