package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
public class LessionDTO extends Lession {
    List<Long> studentListId;

    public LessionDTO() {
        this.studentListId = new ArrayList<>();
    }

    public List<Long> getStudentListId() {
        return studentListId;
    }

    public void setStudentListId(List<Long> studentListId) {
        this.studentListId = studentListId;
    }

    public void setDuration(String duration) {
        try{
            this.setDuration(Long.parseLong(duration));
        }catch (Exception e){
            this.setDuration(1l);
        }
    }
}
