package ru.innopolis.k_sharypov.web.spring_mvc_practice.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

/**
 * Created by innopolis on 06.11.16.
 */
@Configuration
@ComponentScan("ru.innopolis.k_sharypov.web.spring_mvc_practice")

public class RootConfig {
}
