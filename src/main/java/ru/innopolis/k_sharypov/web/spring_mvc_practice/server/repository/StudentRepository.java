package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.StudentEntity;

import java.util.List;

/**
 * Created by innopolis on 09.11.16.
 */
@Repository
@Qualifier("studentDAO")
public interface StudentRepository extends CrudRepository<StudentEntity, Long>, PagingAndSortingRepository<StudentEntity, Long> {
    StudentEntity getById(Long id);

    Page<StudentEntity> findByFirstName (String firstName, Pageable pagable);
}
