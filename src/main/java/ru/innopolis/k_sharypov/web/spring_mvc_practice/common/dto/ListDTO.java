package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by innopolis on 31.10.16.
 */
public class ListDTO {
    private String orderField;
    private String orderDirection;
    private String nameFilter;
    private Long offset;

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }
    public static ListDTO getListDTO(HttpServletRequest req) {
        ListDTO l = new ListDTO();
        l.setNameFilter(req.getParameter("nameFilter"));
        if("".equals(l.getNameFilter())){
            l.setNameFilter(null);
        }
        l.setOrderField(req.getParameter("orderField"));
        if(l.getOrderField()==null || "".equals(l.getOrderField())){
            l.setOrderField("id");
        }
        l.setOrderDirection(req.getParameter("orderDirection"));
        if(l.getOrderDirection()==null || "".equals(l.getOrderDirection())){
            l.setOrderDirection("asc");
        }
        try {
            l.setOffset(Long.parseLong(req.getParameter("offset")));
        }catch (Exception e){
            l.setOffset(0l);
        }
        return l;
    }
}
