package ru.innopolis.k_sharypov.web.spring_mvc_practice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.StudentServiceInterface;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by innopolis on 06.11.16.
 */
@Configuration
@EnableWebMvc
@ComponentScan("ru.innopolis.k_sharypov.web.spring_mvc_practice")

public class WebConfig extends WebMvcConfigurerAdapter{

    @Autowired
    AuthenticationProvider auth_provider;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/WEB-INF/resources/**").addResourceLocations("/resources/"); // register resource link to container
    }

    @Bean
    public InternalResourceViewResolver setupViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

    @Bean
    @Qualifier("studentServiceProxy")
    public StudentServiceInterface getStudentServiceProxy(){
        return  (StudentServiceInterface) Proxy.newProxyInstance(StudentServiceInterface.class.getClassLoader(), new Class[]{StudentServiceInterface.class}, new InvocationHandler() {
            @Autowired
            @Qualifier("studentService")
            private StudentServiceInterface studentServiceInterface;

            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                Object res = null;
                System.out.println("Proxyed "+method.getName());
               switch (method.getName()){
                   case "getList":
                       res = studentServiceInterface.getList( (ListDTO) objects[0]);
                       break;
                   case "getById":
                       res = studentServiceInterface.getById((Long) objects[0]);
                       break;
                   case "delete":
                       res = studentServiceInterface.delete((Long) objects[0]);
                       break;
                   case "save":
                       res = studentServiceInterface.save((Long) objects[0], (StudentDTO) objects[1]);
                       break;
               }
               return res;
            }
        });
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

  /*  @Bean
    public UserDetailsService getUserDetailsService(){
        return new UserDetailsServiceImpl();
    }*/
}

