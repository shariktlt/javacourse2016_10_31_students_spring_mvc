package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.dao;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.LessionDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.LessionStudentDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.EntityManagerFactoryInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.factory.MapperFactoryInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity.LessionEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 01.11.16.
 */
@Repository
public class LessionDAO implements LessionDAOInterface {
    private Long perPage = 1000L;
    private static Logger logger = LoggerFactory.getLogger(LessionDAO.class);


    MapperFactoryInterface mapperFactory;
    MapperFacade mapperFacade;

    @Autowired
    public void setMapperFactory(MapperFactoryInterface mapperFactory){
        this.mapperFactory = mapperFactory;
        mapperFactory.getFactory().classMap(LessionEntity.class, Lession.class)
                .byDefault()
                .register();
        mapperFactory.getFactory().classMap(LessionDTO.class, LessionEntity.class)
                .byDefault()
                .register();
        mapperFacade = mapperFactory.getFactory().getMapperFacade();
    }

    @Autowired
    DbPool dbPool;

    @Autowired
    EntityManagerFactoryInterface entityManagerFactory;

    @Autowired
    LessionStudentDAOInterface lessionStudentDAOInterface;

    @Override
    public Long save(LessionDTO lession) {
        EntityManager em = entityManagerFactory.getEm();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        LessionEntity entity = mapperFacade.map(lession, LessionEntity.class);
        LessionEntity res = em.merge(entity);
        transaction.commit();

        return res.getId();
    }

    @Override
    public List<Lession> getList(ListDTO listDTO) {
        EntityManager em = entityManagerFactory.getEm();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<LessionEntity> criteria = cb.createQuery(LessionEntity.class);
        Root<LessionEntity> root = criteria.from(LessionEntity.class);
        criteria.select(root);
        if(listDTO.getNameFilter()!=null && !listDTO.getNameFilter().isEmpty()){
            criteria.where(cb.equal(root.get("topic"), listDTO.getNameFilter()));
        }
        Query query = em.createQuery(criteria);
        query.setFirstResult(listDTO.getOffset().intValue());
        query.setMaxResults(perPage.intValue());
        List<LessionEntity> list = query.getResultList();
        return mapperFacade.mapAsList(list, Lession.class);
    }

    /*  @Override
        public List<Lession> getList(ListDTO listDTO) {
            List<Lession> list = new ArrayList<>();
            try (Connection conn = dbPool.getConn();
                 PreparedStatement st = conn.prepareStatement(getSql(listDTO))) {
                int index = 0;
                if (listDTO.getNameFilter() != null) {
                    st.setString(++index, listDTO.getNameFilter());
                }
                st.setLong(++index, perPage);
                st.setLong(++index, listDTO.getOffset());
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    list.add(buildLession(rs, false));
                }
            } catch (SQLException e) {
                logger.error("getListStudents exception:", e);
            }
            return list;
        }
    */
    public Lession buildLession(ResultSet rs, boolean isNeedJoin) throws SQLException {
        Lession lession = new Lession();
        lession.setId(rs.getLong("id"));
        lession.setTopic(rs.getString("topic"));
        lession.setDescription(rs.getString("description"));
        lession.setDuration(rs.getLong("duration"));
        lession.setDate(new SimpleDateFormat("dd-MM-yyyy").format(rs.getDate("date")));
        if(isNeedJoin){
            lession.setStudentList(lessionStudentDAOInterface.getStudentsByLessionId(lession.getId()));
        }
        return lession;
    }

    private String getSql(ListDTO listDTO) {
        StringBuilder s = new StringBuilder();
        s.append("SELECT ls.id, ls.topic, ls.description, ls.duration, ls.date FROM lession ls");
        if (listDTO.getNameFilter() != null) {
            s.append("WHERE ls.topic= ?");
        }
        s.append(" ORDER BY ");
        switch (listDTO.getOrderField()) {
            case "topic":
            case "duration":
            case "date":
                s.append(listDTO.getOrderField());
                break;
            default:
                s.append("id");
                break;
        }
        s.append(" ");
        if ("desc".equals(listDTO.getOrderDirection())) {
            s.append("DESC");
        } else {
            s.append("ASC");
        }
        s.append(" LIMIT ? OFFSET ?");
        return s.toString();
    }

    @Override
    public Lession getById(Long id) {
        Lession lession = null;
        LessionEntity entity = entityManagerFactory.getEm().find(LessionEntity.class, id);
        return mapperFacade.map(entity, Lession.class);
    }

    @Override
    public Long add(LessionDTO lessionDTO) {
        Long id = null;
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("INSERT INTO lession (topic, description, duration, date) VALUES (?, ?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            fillLessionStatement(lessionDTO, st);
            int affectedRows = st.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating student failed");
            }
            try (ResultSet generatedKeys = st.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException e) {
            logger.error("add exception", e);
        }
        return id;
    }

    @Override
    public Long update(Long id, LessionDTO lessionDTO) {
        Long res = null;
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("UPDATE lession SET topic = ?, description = ?, duration = ?, date = ? WHERE id = ?")) {
            fillLessionStatement(lessionDTO, st);
            st.setLong(5, id);
            st.executeUpdate();
            res = id;
        } catch (SQLException e) {
            logger.error("update exception", e);
        }
        return res;
    }

    private void fillLessionStatement(LessionDTO lessionDTO, PreparedStatement st) throws SQLException {
        st.setString(1, lessionDTO.getTopic());
        st.setString(2, lessionDTO.getDescription());
        st.setLong(3, lessionDTO.getDuration());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            st.setDate(4, new Date(sdf.parse(lessionDTO.getDate()).getTime()));
        } catch (ParseException e) {
            logger.warn("Invalid date format on fillStudentStatement {}",lessionDTO.getDate());
            st.setDate(4, new Date(0));
        }
    }

    @Override
    public boolean deleteById(Long id) {
        boolean res = false;
        try (Connection conn = dbPool.getConn();
             PreparedStatement st = conn.prepareStatement("DELETE FROM lessions WHERE id = ?")) {
            st.setLong(1, id);
            res = st.execute();
        }catch (SQLException e){
            logger.error("deleteById exception",e);
        }
        return res;
    }
}
