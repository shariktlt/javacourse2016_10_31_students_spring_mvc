package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dao.LessionDAOInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.LessionDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Lession;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.LessionServiceInterface;

import java.util.List;

/**
 * Created by innopolis on 31.10.16.
 */
@Service("lessionService")
public class LessionService implements LessionServiceInterface {
    @Autowired
    LessionDAOInterface lessionDAOInterface;

    @Override
    public List<Lession> getList(ListDTO listDTO){
        return lessionDAOInterface.getList(listDTO);
    }

    @Override
    public Lession getById(Long id) {
        Lession lession = lessionDAOInterface.getById(id);
        if(lession == null){
            lession = new Lession();
            lession.setTopic("");
            lession.setDescription("");
            lession.setDuration(1L);
            lession.setDate("");
            lession.setId(0l);
        }
        return lession;
    }

    @Override
    public Long save(Long id, LessionDTO lessionDTO) {
        Long res = null;
        /*if(id > 0l){
            res = lessionDAOInterface.update(id, lessionDTO);
        }else{
            res = lessionDAOInterface.add(lessionDTO);
        }*/
        lessionDTO.setId(id);
        res = lessionDAOInterface.save(lessionDTO);
        return res;
    }

    @Override
    public boolean delete(Long id) {
        return lessionDAOInterface.deleteById(id);
    }
}
