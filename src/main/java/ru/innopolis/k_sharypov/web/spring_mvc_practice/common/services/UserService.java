package ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services;

import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.User;

/**
 * Created by innopolis on 06.11.16.
 */
public interface UserService {
    User getUser(String login);
}
