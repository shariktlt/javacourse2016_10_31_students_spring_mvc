package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.decorators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.ListDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.dto.StudentDTO;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.entitys.Student;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.common.services.StudentServiceInterface;
import ru.innopolis.k_sharypov.web.spring_mvc_practice.server.service.StudentService;

import java.security.Principal;
import java.util.List;

/**
 * Created by innopolis on 15.11.16.
 */
@Service
@Qualifier("studentServiceWithLogger")
public class StudentServiceLoggerDecorator implements StudentServiceInterface {

    private static Logger logger = LoggerFactory.getLogger(StudentServiceLoggerDecorator.class);

   /* SecurityContext securityContext;

    public StudentServiceLoggerDecorator(SecurityContext securityContext) {
        this.securityContext = securityContext;
    }*/

    @Autowired
    StudentServiceInterface studentService;

    @Override
    public List<Student> getList(ListDTO listDTO) {
        logger.info("getList by "+getUserName());
        return studentService.getList(listDTO);
    }

    @Override
    public Student getById(Long id) {
        logger.info("getById by "+getUserName());
        return studentService.getById(id);
    }

    @Override
    public Long save(Long id, StudentDTO studentDTO) {
        logger.info("save by "+getUserName());
        return studentService.save(id, studentDTO);
    }

    @Override
    public boolean delete(Long id) {
        logger.info("delete by "+getUserName());
        return studentService.delete(id);
    }

    private String getUserName() {
        String name = "unknown";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null){
            name = auth.getName();
        }
        return name;
    }


}
