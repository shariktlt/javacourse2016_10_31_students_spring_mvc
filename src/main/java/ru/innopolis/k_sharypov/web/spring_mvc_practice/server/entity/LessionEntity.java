package ru.innopolis.k_sharypov.web.spring_mvc_practice.server.entity;

import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by innopolis on 07.11.16.
 */
@Entity
@Table(name="lession")
public class LessionEntity {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="topic")
    private String topic;

    @Column(name="description")
    private String description;

    @Column(name="duration")
    private Long duration;

    @Column(name="date")
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
