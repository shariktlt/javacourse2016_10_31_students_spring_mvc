<%--
  Created by IntelliJ IDEA.
  User: innopolis
  Date: 31.10.16
  Time: 12:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Student form</title>
</head>
<body>
    <form method="POST">
        <label>First name</label>
        <input name="firstName" placeholder="firstName" value="${student.firstName}">
        <br>
        <label>Last name</label>
        <input name="lastName" placeholder="lastName" value="${student.lastName}">
        <br>
        <label>Sex</label>
        <select name="sex">
            <option value="m" <c:if test="${student.sex == 'm'}">selected="selected"</c:if> >male</option>
            <option value="f" <c:if test="${student.sex == 'f'}">selected="selected"</c:if> >female</option>
        </select>
        <br>
        <input name="birth" placeholder="dd-mm-yyyy" value="${student.birth}"><br>

        <select multiple="multiple" name="lessions_id">
            <c:forEach items="${lessions}" var="item">
                <option value="${item.id}" <c:if test="${student.lessionListIdMap.containsKey(item.id)}">selected="selected"</c:if> >${item.topic}</option>
            </c:forEach>
        </select>
        <br>
        <input type="submit" value="${student.id>0?'Update':'Add'}">
        <a href="${pageContext.request.contextPath}/app/student/">Back to list</a>
    </form>
    <t:menu/>
</body>
</html>
