<%--
  Created by IntelliJ IDEA.
  User: innopolis
  Date: 31.10.16
  Time: 12:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Student form</title>
</head>
<body>
    <form method="POST">
        <label>Topic</label>
        <input name="topic" placeholder="topic" value="${lession.topic}">
        <br>
        <label>Description</label>
        <textarea name="description" placeholder="description">${lession.description}</textarea>
        <br>
        <label>Duration</label>
        <input name="duration" type="number" min="1" max="10000"  value="${lession.duration}">
        <br>
        <input name="date" placeholder="dd-mm-yyyy" value="${lession.date}"><br>
        <input type="submit" value="${lession.id>0?'Update':'Add'}">
        <a href="/app/lessions/">Back to list</a>
    </form>
    <t:menu/>
</body>
</html>
