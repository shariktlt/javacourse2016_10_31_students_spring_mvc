<%@tag description="Menu" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
    <a href="/app/student/">Students</a> <a href="/app/lessions/">Lession</a> <sec:authorize access="isAuthenticated()">
    <a href="<c:url value="/logout" />">Logout</a>
</sec:authorize>
</div>